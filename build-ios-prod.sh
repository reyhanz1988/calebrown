#!/bin/sh
cd ios
#xcodebuild -workspace Calebrown.xcworkspace \
#  -scheme Calebrown clean archive -configuration release \
#  -sdk iphoneos -archivePath Calebrown.xcarchive

xcodebuild -exportArchive -archivePath  Calebrown.xcarchive \
  -allowProvisioningUpdates true \
  -exportOptionsPlist  Calebrown/exportOptions.plist \
  -exportPath  Calebrown.ipa