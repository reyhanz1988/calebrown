import React from 'react';
import { Image, StyleSheet } from 'react-native';
import { IconButton } from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { createStackNavigator } from 'react-navigation-stack';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import CryptoScreen from '../screens/CryptoScreen';
import CategoriesScreen from '../screens/CategoriesScreen';
import ExchangesScreen from '../screens/ExchangesScreen';
import DerivativesScreen from '../screens/DerivativesScreen';
import PreviewScreen from '../screens/PreviewScreen';

import SettingsScreen from '../screens/SettingsScreen';
import ChangeLanguageScreen from '../screens/ChangeLanguageScreen';
import ChangeCurrencyScreen from '../screens/ChangeCurrencyScreen';
import GlobalDataScreen from '../screens/GlobalDataScreen';

import languages from '../screensTranslations/AppStack';
import {FIRST_COLOR,SECOND_COLOR} from "@env";

let CryptoStack = createStackNavigator({
    Crypto: CryptoScreen,
    Preview: PreviewScreen
});
let CategoriesStack = createStackNavigator({
    Categories: CategoriesScreen,
    Preview: PreviewScreen
});
let ExchangesStack = createStackNavigator({
    Exchanges: ExchangesScreen,
    Preview: PreviewScreen
});
let DerivativesStack = createStackNavigator({
    Derivatives: DerivativesScreen,
    Preview: PreviewScreen
});
let SettingsStack = createStackNavigator({
    Settings: SettingsScreen,
    ChangeLanguage: ChangeLanguageScreen,
    ChangeCurrency: ChangeCurrencyScreen,
    GlobalData: GlobalDataScreen,
    Preview: PreviewScreen
});
const styles = StyleSheet.create({
    icons: {
        width: 24,
        height: 24,
        resizeMode: 'contain',
    },
});

async function getLang() {
    let currentLang = await AsyncStorage.getItem('LANG');
    if(currentLang == '' || currentLang == null || currentLang == undefined){
        currentLang = 'en';
    }
    CryptoStack.navigationOptions = ({ navigation }) => {
        return {
            tabBarLabel: languages[0][currentLang],
            tabBarIcon: ({ focused }) => (
                focused ? <IconButton style={{marginTop:-10,marginLeft:-10}} size={28} icon='home' color='#FADA5E' /> : <IconButton style={{marginTop:-10,marginLeft:-10}} size={28} icon='home' color='#ffffff' />
            ),
            tabBarOptions: {
                showIcon: true,
                upperCaseLabel: false,
                indicatorStyle:{borderBottomWidth:0,backgroundColor: 'transparent',height:0},
                tabStyle: {backgroundColor: FIRST_COLOR,height:70},
                activeTintColor: '#FADA5E',
                inactiveTintColor: '#ffffff',
                labelStyle :{fontSize:12,width:76,marginTop:2,}
            }
        }
    };
    CategoriesStack.navigationOptions = ({ navigation }) => {
        return {
            tabBarLabel: languages[1][currentLang],
            tabBarIcon: ({ focused }) => (
                focused ? <IconButton style={{marginTop:-10,marginLeft:-10}} size={28} icon='book-open-variant' color='#FADA5E' /> : <IconButton style={{marginTop:-10,marginLeft:-10}} size={28} icon='book-open-variant' color='#ffffff' />
            ),
            tabBarOptions: {
                showIcon: true,
                upperCaseLabel: false,
                indicatorStyle:{borderBottomWidth:0,backgroundColor: 'transparent',height:0},
                tabStyle: {backgroundColor: FIRST_COLOR,height:70},
                activeTintColor: '#FADA5E',
                inactiveTintColor: '#ffffff',
                labelStyle :{fontSize:12,width:76,marginTop:2,}
            }
        }
    };
    ExchangesStack.navigationOptions = ({ navigation }) => {
        return {
            tabBarLabel: languages[2][currentLang],
            tabBarIcon: ({ focused }) => (
                focused ? <IconButton style={{marginTop:-10,marginLeft:-10}} size={28} icon='cash-100' color='#FADA5E' /> : <IconButton style={{marginTop:-10,marginLeft:-10}} size={28} icon='cash-100' color='#ffffff' />
            ),
            tabBarOptions: {
                showIcon: true,
                upperCaseLabel: false,
                indicatorStyle:{borderBottomWidth:0,backgroundColor: 'transparent',height:0},
                tabStyle: {backgroundColor: FIRST_COLOR,height:70},
                activeTintColor: '#FADA5E',
                inactiveTintColor: '#ffffff',
                labelStyle :{fontSize:12,width:76,marginTop:2,}
            }
        }
    };
    DerivativesStack.navigationOptions = ({ navigation }) => {
        return {
            tabBarLabel: languages[3][currentLang],
            tabBarIcon: ({ focused }) => (
                focused ? <IconButton style={{marginTop:-10,marginLeft:-10}} size={28} icon='swap-horizontal' color='#FADA5E' /> : <IconButton style={{marginTop:-10,marginLeft:-10}} size={28} icon='swap-horizontal' color='#ffffff' />
            ),
            tabBarOptions: {
                showIcon: true,
                upperCaseLabel: false,
                indicatorStyle:{borderBottomWidth:0,backgroundColor: 'transparent',height:0},
                tabStyle: {backgroundColor: FIRST_COLOR,height:70},
                activeTintColor: '#FADA5E',
                inactiveTintColor: '#ffffff',
                labelStyle :{fontSize:12,width:76,marginTop:2,}
            }
        }
    };
    SettingsStack.navigationOptions = ({ navigation }) => {
        return {
            tabBarLabel: languages[4][currentLang],
            tabBarIcon: ({ focused }) => (
                focused ? <IconButton style={{marginTop:-10,marginLeft:-10}} size={28} icon='cog' color='#FADA5E' /> : <IconButton style={{marginTop:-10,marginLeft:-10}} size={28} icon='cog' color='#ffffff' />
            ),
            tabBarOptions: {
                showIcon: true,
                upperCaseLabel: false,
                indicatorStyle:{borderBottomWidth:0,backgroundColor: 'transparent',height:0},
                tabStyle: {backgroundColor: FIRST_COLOR,height:70},
                activeTintColor: '#FADA5E',
                inactiveTintColor: '#ffffff',
                labelStyle :{fontSize:12,width:76,marginTop:2,}
            }
        }
    };
}
getLang();

const AppStack = createMaterialTopTabNavigator(
{
    CryptoStack,
    CategoriesStack,
    ExchangesStack,
    DerivativesStack,
    SettingsStack,
}, 
{
    swipeEnabled: false,
    animationEnabled: true,
    tabBarPosition:'bottom',
});

export {AppStack}