import React from 'react';
import { Platform } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AuthLoadingScreen from '../screens/AuthLoadingScreen';
import {AppStack} from '../navigation/AppStack';

export default createAppContainer(
    createSwitchNavigator({
        AuthLoading: AuthLoadingScreen,
        App: AppStack
    },
    {
        initialRouteName: 'AuthLoading',
    })
);