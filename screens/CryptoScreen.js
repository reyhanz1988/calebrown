import React from 'react';
import { connect } from 'react-redux';
import * as productActions from '../actions/productActions';
import { Col, Row, Grid } from "react-native-easy-grid";
import { Dimensions, Image, RefreshControl, ScrollView, StyleSheet, Text, View } from 'react-native';
import { ActivityIndicator, Appbar, Button, Card, Paragraph, Searchbar, Title } from 'react-native-paper';
import Reload from '../components/Reload';
import NumberFormat from 'react-number-format';
import AsyncStorage from '@react-native-async-storage/async-storage';
import logo from '../assets/images/logo.png';
import Loading from '../components/Loading';
import languages from '../screensTranslations/Crypto';
import {BASE_URL,FIRST_COLOR,SECOND_COLOR} from "@env";


class CryptoScreen extends React.Component {
    componentDidMount() {
        this.setState({
            currentLang: this.props.screenProps.currentLang,
            currentCurrency: this.props.screenProps.currentCurrency,
        }, () =>{
            this.getData();
        });
    }
    componentDidUpdate(prevprops) {
        if(prevprops.checkPingRes != this.props.checkPingRes){
            if(this.props.checkPingRes == 'error'){
                this.setState({
                    getDataError: languages[8][this.state.currentLang],
                    isLoading: false
                });
            }
            else{
                if(this.props.checkPingRes.status_msg){
                    this.setState({
                        getDataError: this.props.checkPingRes.status_msg,
                        isLoading: false
                    });
                }
                else{
                    this.setState({ 
                        currentPage: 1,
                        refresh: true,
                        getCoins: []
                    }, () =>{
                        let vars = this.state;
                        this.props.getGlobal(vars);
                        this.props.getCoins(vars);
                    });
                }
            }
        }
        if(prevprops.getGlobalRes != this.props.getGlobalRes){
            if(this.props.getGlobalRes == 'error'){
                this.setState({
                    getDataError: this.props.getGlobalRes,
                    isLoading: false
                });
            }
            else{
                if(this.props.getGlobalRes.status_msg){
                    this.setState({
                        getDataError: this.props.getGlobalRes.status_msg,
                        isLoading: false
                    });
                }
                else{
                    let activeCrypto    = this.props.getGlobalRes.data.active_cryptocurrencies;
                    let perPage         = this.state.perPage;
                    let maxPage         = Math.ceil(activeCrypto/perPage);
                    let searchPerPage   = this.state.searchPerPage;
                    let maxSearchPage   = Math.ceil(activeCrypto/searchPerPage);
                    this.setState({activeCrypto: activeCrypto, maxPage: maxPage, maxSearchPage: maxSearchPage});
                }
            }
        }
        if(prevprops.getCoinsRes !== this.props.getCoinsRes){
            if(this.props.getCoinsRes == 'error'){
                this.setState({
                    getDataError: this.props.getCoinsRes,
                    isLoading: false,
                    refresh: false
                });
            }
            else{
                if(this.props.getCoinsRes.status_msg){
                    this.setState({
                        getDataError: this.props.getCoinsRes.status_msg,
                        isLoading: false,
                        refresh: false
                    });
                }
                else{
                    if(this.state.currentPage == 1){
                        this.setState({ getDataError: '',getCoins: this.props.getCoinsRes,isLoading: false, refresh: false, searchData: false });
                    }
                    else{
                        if(this.props.getCoinsRes.length > 0){
                            let concatData = [...this.state.getCoins, ...this.props.getCoinsRes];
                            this.setState({ getCoins: concatData }, () => {
                                this.setState({moreLoading: false, searchData: false});
                            });
                        }
                        else{
                            this.setState({moreLoading: false, searchData: false});
                        }
                    }
                }
            }
        }
        if(prevprops.searchCoinsRes !== this.props.searchCoinsRes){
            if(this.props.searchCoinsRes == 'error'){
                this.setState({
                    getDataError: this.props.searchCoinsRes,
                    isLoading: false,
                    refresh: false
                });
            }
            else{
                if(this.props.searchCoinsRes.status_msg){
                    this.setState({
                        getDataError: this.props.searchCoinsRes.status_msg,
                        isLoading: false,
                        refresh: false
                    });
                }
                else{
                    let data = this.props.searchCoinsRes;
                    let searchCoins = this.state.searchCoins;
                    if(data.length > 0){
                        for(let i=0; i<data.length; i++){
                            let symbol = data[i].symbol
                            if(symbol.search(this.state.searchText) >= 0){
                                searchCoins.push(data[i]);
                            }
                        }
                    }
                    if(this.state.searchPage == 1){
                        this.setState({ getDataError: '',searchCoins:searchCoins, getCoins: searchCoins,searchLoading: false, searchData: true });
                    }
                    else{
                        if(this.props.searchCoinsRes.length > 0){
                            let concatData = [...this.state.getCoins, ...searchCoins];
                            this.setState({ getCoins: concatData }, () => {
                                this.setState({moreLoading: false, searchData: true});
                            });
                        }
                        else{
                            this.setState({moreLoading: false, searchData: true});
                        }
                    }
                }
            }
        }
    }
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            refresh: false,
            moreLoading:false,
            searchLoading:false,
            currentLang: 'en',
            currentCurrency: 'USD',
            getCoins: [],
            order: 'market_cap_desc',
            searchText: '',
            typingTimeout: 0,
            activeCrypto:0,
            maxPage:50,
            searchCoins: [],
            searchData:false,
            currentPage: 1,
            searchPage:1,
            perPage: 30,
            maxSearchPage:50,
            searchPerPage:250,
        };
        this.getData = this.getData.bind(this);
        this.getMoreData = this.getMoreData.bind(this);
        this.getMoreDataSearch = this.getMoreDataSearch.bind(this);
        this.searchCoins = this.searchCoins.bind(this);
        this.formatter = this.formatter.bind(this);
    }
    getData(){
        this.props.checkPing();
    }
    getMoreData(){
        if(this.state.moreLoading == false){
            let currentPage = this.state.currentPage;
            this.setState({ moreLoading:true, currentPage: (Number(currentPage)+1) }, () => {
                let vars = this.state;
                this.props.getCoins(vars);
            });
        }
    }
    searchCoins(searchText){
        if (this.state.typingTimeout) {
            clearTimeout(this.state.typingTimeout);
        }
        this.setState({
            searchText:searchText,
            typingTimeout: setTimeout(() => {
                if(searchText.length > 1){
                    this.setState({searchCoins:[],searchLoading:true,searchPage:1}, () => {
                        let vars = this.state;
                        this.props.searchCoins(vars);
                    });
                }
                else if(searchText.length == 0){
                    this.setState({searchCoins:[],searchLoading:false,searchData:false}, () => {
                        this.getData();
                    });
                }
            }, 1000)
        });
    }
    getMoreDataSearch(){
        if(this.state.moreLoading == false && this.state.searchData == true){
            let searchPage = this.state.searchPage;
            this.setState({ searchCoins:[],moreLoading:true, searchPage: (Number(searchPage)+1) }, () => {
                let vars = this.state;
                this.props.searchCoins(vars);
            });
        }
    }
    formatter(data,fontSize){
        let num = Math.round((data + Number.EPSILON) * 100) / 100;
        let res = (<NumberFormat 
                        displayType={'text'} 
                        thousandSeparator={true} 
                        value={num} 
                        renderText={(value, props) => <Text style={{textAlign:'right',fontSize:fontSize}}>{value}</Text> }/>);
        return res;
    }
    render() {
        if(this.state.isLoading == true){
            return (
                <Loading />
            );
        }
        else if(this.state.getDataError != ''){
            return (
                <Reload errorMessage={this.state.getDataError} getData={this.getData} />
            );
        }
        else{
            let coins = this.state.getCoins;
            let currentCurrency = this.state.currentCurrency;
            let viewData;
            let moreData;
            if(coins.length > 0){
                if(this.state.searchData){
                    if(this.state.searchPage <= this.state.maxSearchPage && !this.state.moreLoading){
                        moreData = (
                            <Button style={styles.selectedButton} labelStyle={styles.selectedButtonText} uppercase={false} mode="contained" onPress={() => this.getMoreDataSearch()}>
                                {languages[4][this.state.currentLang]}
                            </Button>
                        );
                    }
                }
                else{
                    if(this.state.currentPage <= this.state.maxPage && !this.state.moreLoading){
                        moreData = (
                            <Button style={styles.selectedButton} labelStyle={styles.selectedButtonText} uppercase={false} mode="contained" onPress={() => this.getMoreData()}>
                                {languages[5][this.state.currentLang]}
                            </Button>
                        );
                    }
                }
                viewData = (
                    <Grid>
                        <Row style={{height:40,paddingTop:10,backgroundColor:'#222'}}>
                            <Col style={{width:50}}><Text style={{fontSize:12,textAlign:'center',color:'#fff'}}>#</Text></Col>
                            <Col style={{width:80}}><Text style={{fontSize:12,textAlign:'center',color:'#fff'}}>{languages[1][this.state.currentLang]}</Text></Col>
                            <Col style={{width:90}}><Text style={{fontSize:12,textAlign:'right',color:'#fff'}}>{languages[2][this.state.currentLang]}</Text></Col>
                            <Col style={{width:50}}><Text style={{fontSize:12,textAlign:'right',color:'#fff'}}>24H</Text></Col>
                            <Col><Text style={{paddingRight:10,fontSize:12,textAlign:'right',color:'#fff'}}>{languages[3][this.state.currentLang]}</Text></Col>
                        </Row>
                        <ScrollView refreshControl={<RefreshControl refreshing={this.state.refresh} onRefresh={()=>this.getData()} />}>
                        {
                            coins.map((item, i) => (
                                <Row key={'row_'+i} style={(i%2) ? (styles.rowsColor) : styles.rowsWhite}>
                                    <Col style={{width:50}}><Text style={{fontSize:12,textAlign:'center'}}>{(i+1)}</Text></Col>
                                    <Col style={{width:80,flexDirection:'row',justifyContent:'center'}}>
                                        <Image source={{uri:item.image}} style={{width:20,height:20,marginRight:5,marginTop:0}}/>
                                        <Text style={{fontSize:12,textAlign:'center',width:60}}>{item.symbol.toUpperCase()}</Text>
                                    </Col>
                                    <Col style={{width:90}}>{this.formatter((item.current_price),9)}</Col>
                                    <Col style={{width:50}}>
                                        <Text style={[styles.greenText, item.price_change_percentage_24h<=0 && styles.redText]}>
                                            {(item.price_change_percentage_24h) ? (item.price_change_percentage_24h).toFixed(1)+'%' : '0.0%'}
                                        </Text>
                                    </Col>
                                    <Col style={{paddingRight:10}}>{this.formatter((item.market_cap),9)}</Col>
                                </Row>
                            ))
                        }
                        </ScrollView>
                    </Grid>
                );
            }
            let bottomLoading;
            if(this.state.moreLoading == true){
                bottomLoading = (
                    <ActivityIndicator />
                );
            }
            return (
                <View style={styles.wrapper}>
                    <Appbar.Header style={{backgroundColor:FIRST_COLOR}}>
                        <Appbar.Content titleStyle={{color:'#fff',textAlign:'center'}} title={languages[0][this.state.currentLang]} />
                    </Appbar.Header>
                    <View style={{flexDirection:'row',marginBottom:10}}>
                        <Card style={{marginRight:5,width:Dimensions.get('window').width * 0.5}}>
                            <Card.Content>
                                <Title style={{fontSize:14}}>{languages[6][this.state.currentLang]}</Title>
                                <Paragraph style={{color:'green'}}>{this.formatter((this.state.activeCrypto),14)}</Paragraph>
                            </Card.Content>
                        </Card>
                        <Card style={{marginLeft:5,width:Dimensions.get('window').width * 0.5}}>
                            <Card.Content>
                            <Title style={{fontSize:14}}>{languages[7][this.state.currentLang]}</Title>
                                <Paragraph style={{color:'green'}}>{this.state.currentCurrency}</Paragraph>
                            </Card.Content>
                        </Card>
                    </View>
                    <Searchbar
                        placeholder={languages[8][this.state.currentLang]}
                        onChangeText={(text)=>this.searchCoins(text)}
                        value={this.state.searchText}
                        style={{margin:15}}
                    />
                    {viewData}
                    {moreData}
                    {bottomLoading}
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    wrapper:{
        flex:1,
    },
    header:{
        width: Dimensions.get('window').width * 1,
        backgroundColor:FIRST_COLOR,
        height:90,
        alignItems:'center'
    },
    logo:{
        width:305,
        height:87,
    },
    rowsWhite: { 
        height:50,paddingTop:10,borderTopWidth:1,borderTopColor:'#ddd',backgroundColor:'#fff'
    },
    rowsColor:{
        height:50,paddingTop:10,borderTopWidth:1,borderTopColor:'#ddd',backgroundColor: '#F7F6E7'
    },
    greenText: { 
        fontSize:12,textAlign:'right',color:'green'
    },
    redText: { 
        fontSize:12,textAlign:'right',color:'red'
    },
    selectedButton:{
        margin:5,
        backgroundColor:FIRST_COLOR
    },
    selectedButtonText:{
        fontSize:11
    }
});

CryptoScreen.navigationOptions = {
    header:null
};

function mapStateToProps(state, props) {
    return {
        checkPingRes: state.productReducer.checkPingRes,
        getGlobalRes: state.productReducer.getGlobalRes,
        getCoinsRes: state.productReducer.getCoinsRes,
        searchCoinsRes: state.productReducer.searchCoinsRes,
    }
}
const mapDispatchToProps = {
    ...productActions
};
export default connect(mapStateToProps, mapDispatchToProps)(CryptoScreen);