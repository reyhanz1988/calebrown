import React from 'react';
import { connect } from 'react-redux';
import * as productActions from '../actions/productActions';
import { Dimensions, Image, StyleSheet, ScrollView, Text, View } from 'react-native';
import { Appbar, Button, Card, Icon, List, Paragraph, Title} from 'react-native-paper';
import NumberFormat from 'react-number-format';
import AsyncStorage from '@react-native-async-storage/async-storage';
import languages from '../screensTranslations/GlobalData';
import RNRestart from 'react-native-restart';
import Loading from '../components/Loading';
import Reload from '../components/Reload';
import {FIRST_COLOR} from "@env";

class GlobalDataScreen extends React.Component {
    componentDidMount() {
        this.setState({
            currentLang: this.props.screenProps.currentLang,
            currentCurrency: this.props.screenProps.currentCurrency,
        }, () =>{
            this.getData();
        });
    }   
    componentDidUpdate(prevprops) {
        if(prevprops.checkPingRes != this.props.checkPingRes){
            if(this.props.checkPingRes == 'error'){
                this.setState({
                    getDataError: languages[10][this.state.currentLang],
                    isLoading: false
                });
            }
            else{
                if(this.props.checkPingRes.status_msg){
                    this.setState({
                        getDataError: this.props.checkPingRes.status_msg,
                        isLoading: false
                    });
                }
                else{
                    let vars = this.state;
                    this.props.getGlobal(vars);
                }
            }
        }
        if(prevprops.getGlobalRes != this.props.getGlobalRes){
            if(this.props.getGlobalRes == 'error'){
                this.setState({
                    getDataError: this.props.getGlobalRes,
                    isLoading: false
                });
            }
            else{
                if(this.props.getGlobalRes.status_msg){
                    this.setState({
                        getDataError: this.props.getGlobalRes.status_msg,
                        isLoading: false
                    });
                }
                else{
                    this.setState({ 
                        getGlobal: this.props.getGlobalRes.data,
                        isLoading:false
                    });
                }
            }
        }
    }
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            currentShop: '',
            currentLang: '',
            activeTab:0,
            getGlobal:[],
            getDataError:''
        };
        this.getData = this.getData.bind(this);
        this.formatter = this.formatter.bind(this);
        this.switchTab = this.switchTab.bind(this);
        this.renderActivetab = this.renderActivetab.bind(this);
        this.renderMarketCap = this.renderMarketCap.bind(this);
        this.renderMarketCapPercentage = this.renderMarketCapPercentage.bind(this);
        this.renderTotalVolume = this.renderTotalVolume.bind(this);
        this.renderMainComponent = this.renderMainComponent.bind(this);
    }
    getData(){
        this.props.checkPing();
    }
    formatter(data,suffix){
        let num = Math.round((data + Number.EPSILON) * 100) / 100;
        let res = (<NumberFormat displayType={'text'} thousandSeparator={true} value={num} suffix={suffix} renderText={(value, props) => <Text {...props}>{value}</Text> }/>);
        return res;
    }
    switchTab(nextTab){
        this.setState({activeTab: nextTab});
    }
    renderActivetab() {
        switch(this.state.activeTab) {
            case 0:
                return this.renderMarketCap();
            case 1:
                return this.renderMarketCapPercentage();
            case 2:
                return this.renderTotalVolume();
        }
    }
    renderMarketCap(){
        let data = this.state.getGlobal.total_market_cap;
        return this.renderMainComponent('total_market_cap',data);
    }
    renderMarketCapPercentage(){
        let data = this.state.getGlobal.market_cap_percentage;
        return this.renderMainComponent('market_cap_percentage',data);
    }
    renderTotalVolume(){
        let data = this.state.getGlobal.total_volume;
        return this.renderMainComponent('total_volume',data);
    }
    renderMainComponent(dataType,data){
        let listViews = [];
        for (let i in data) {
            let leftIcons = [];
            let iconUri = "https://cryptoicon-api.vercel.app/api/icon/"+i;
            let flagUri = "https://flagcdn.com/28x21/"+i.substring(0,2)+".png";
            let displayData = [];
            if(dataType == 'market_cap_percentage'){
                displayData.push(this.formatter(data[i],'%'));
                leftIcons.push(
                    <View key={'cr_'+i} style={{width:50,alignItems:'center'}}>
                        <Image source={{uri:iconUri}} style={{width:30,height:30}}/>
                    </View>
                );
            }
            else{
                displayData.push(this.formatter(data[i],''));
                if(i == 'btc' || i == 'eth' || i == 'ltc' || i == 'bch'|| i == 'bnb'|| i == 'eos'|| i == 'xrp'|| i == 'xlm'|| 
                i == 'link'|| i == 'dot'|| i == 'yfi'|| i == 'xdr'|| i == 'xag'|| i == 'xau'|| i == 'bits'|| i == 'sats'){
                        leftIcons.push(
                            <View key={'cr_'+i} style={{width:50,alignItems:'center'}}>
                                <Image source={{uri:iconUri}} style={{width:30,height:30}}/>
                            </View>
                        );
                }
                else{
                    leftIcons.push(
                        <View key={'cr_'+i} style={{width:50,alignItems:'center'}}>
                            <Image source={{uri:flagUri}} style={{width:30,height:30}}/>
                        </View>
                    );
                }
            }
            
            let bgColor;
            if(i.toUpperCase() == this.state.currentCurrency){
                bgColor = "#FADA5E";
            }
            else{
                bgColor = "#fff";
            }
            listViews.push(
                <List.Item
                    key={i}
                    title={i.toUpperCase()}
                    description={displayData}
                    descriptionStyle={{color:'green'}}
                    left={props => leftIcons}
                    right={props => <List.Icon {...props} icon='chevron-right' />}
                    style={{borderBottomWidth:1,borderBottomColor:'#eee',backgroundColor:bgColor}}
                />
            );
        }
        return (
            <ScrollView>
                {listViews}
            </ScrollView>
        )
    }
    render() {
        if(this.state.isLoading == true){
            return (
                <Loading />
            );
        }
        else if(this.state.getDataError != ''){
            return (
                <Reload errorMessage={this.state.getDataError} getData={this.getData} />
            );
        }
        else{
            let getGlobal = this.state.getGlobal;
            let listViews = [];
            let updateDate = new Date(getGlobal.updated_at*1000);
            let monthsArr = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
            let lastUpdate = ('0' + updateDate.getDate()).slice(-2)+' '+monthsArr[updateDate.getMonth()]+' '+updateDate.getFullYear();
            let button0,button1,button2;
            let buttonDisplay = [];
            for(let i =0; i<3; i++){
                if(this.state.activeTab == i){
                    buttonDisplay.push(
                        <Button key={'menu'+i} style={styles.selectedButton} labelStyle={styles.selectedButtonText} uppercase={false} mode="contained" onPress={() => {return false}}>
                            {languages[(i+7)][this.state.currentLang]}
                        </Button>
                    )
                }
                else{
                    buttonDisplay.push(
                        <Button key={'menu'+i} style={styles.defaultButton} labelStyle={styles.defaultButtonText} uppercase={false} mode="contained" onPress={() => this.switchTab(i)}>
                            {languages[(i+7)][this.state.currentLang]}
                        </Button>
                    )
                }
            }
            return (
                <View style={styles.wrapper}>
                    <Appbar.Header style={{backgroundColor:FIRST_COLOR}}>
                        <Appbar.BackAction style={{marginTop:10}} color="#fff" onPress={() => this.props.navigation.goBack()} />
                        <Appbar.Content titleStyle={{color:'#fff',textAlign:'center'}} title={languages[0][this.state.currentLang]} />
                    </Appbar.Header>
                    <View style={{flexDirection:'row',marginBottom:10}}>
                        <Card style={{marginRight:5,width:Dimensions.get('window').width * 0.5}}>
                            <Card.Content>
                                <Title style={{fontSize:14}}>{languages[1][this.state.currentLang]}</Title>
                                <Paragraph style={{color:'green'}}>{this.formatter(getGlobal.active_cryptocurrencies,'')}</Paragraph>
                            </Card.Content>
                        </Card>
                        <Card style={{marginLeft:5,width:Dimensions.get('window').width * 0.5}}>
                            <Card.Content>
                            <Title style={{fontSize:14}}>{languages[2][this.state.currentLang]}</Title>
                                <Paragraph style={{color:'green'}}>{this.formatter(getGlobal.markets,'')}</Paragraph>
                            </Card.Content>
                        </Card>
                    </View>
                    <View style={{flexDirection:'row',marginBottom:10}}>
                        <Card style={{marginRight:5,width:Dimensions.get('window').width * 0.5}}>
                            <Card.Content>
                                <Title style={{fontSize:14}}>{languages[3][this.state.currentLang]}</Title>
                                <Paragraph style={{color:'green'}}>{this.formatter(getGlobal.upcoming_icos,'')}</Paragraph>
                            </Card.Content>
                        </Card>
                        <Card style={{marginLeft:5,width:Dimensions.get('window').width * 0.5}}>
                            <Card.Content>
                            <Title style={{fontSize:14}}>{languages[4][this.state.currentLang]}</Title>
                                <Paragraph style={{color:'green'}}>{this.formatter(getGlobal.ongoing_icos,'')}</Paragraph>
                            </Card.Content>
                        </Card>
                    </View>
                    <View style={{flexDirection:'row',marginBottom:10}}>
                        <Card style={{marginRight:5,width:Dimensions.get('window').width * 0.5}}>
                            <Card.Content>
                                <Title style={{fontSize:14}}>{languages[5][this.state.currentLang]}</Title>
                                <Paragraph style={{color:'green'}}>{this.formatter(getGlobal.ended_icos,'')}</Paragraph>
                            </Card.Content>
                        </Card>
                        <Card style={{marginLeft:5,width:Dimensions.get('window').width * 0.5}}>
                            <Card.Content>
                            <Title style={{fontSize:14}}>{languages[6][this.state.currentLang]}</Title>
                                <Paragraph style={{color:'green'}}>{lastUpdate}</Paragraph>
                            </Card.Content>
                        </Card>
                    </View>
                    <View style={{flexDirection:'row',marginBottom:10}}>
                        {buttonDisplay}
                    </View>
                    {this.renderActivetab()}
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        width:'100%',
        height:'100%'
    },
    defaultButton:{
        flex:1,
        marginLeft:5,
        marginRight:5,
        backgroundColor:"#ddd"
    },
    defaultButtonText:{
        fontSize:11,
        color:FIRST_COLOR
    },
    selectedButton:{
        flex:1,
        marginLeft:5,
        marginRight:5,
        backgroundColor:FIRST_COLOR
    },
    selectedButtonText:{
        fontSize:11
    }
});

GlobalDataScreen.navigationOptions = {
    header:null
};

function mapStateToProps(state, props) {
    return {
        checkPingRes: state.productReducer.checkPingRes,
        getGlobalRes: state.productReducer.getGlobalRes
    }
}
const mapDispatchToProps = {
    ...productActions
};
export default connect(mapStateToProps, mapDispatchToProps)(GlobalDataScreen);