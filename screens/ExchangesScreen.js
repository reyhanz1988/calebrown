import React from 'react';
import { connect } from 'react-redux';
import * as productActions from '../actions/productActions';
import { Col, Row, Grid } from "react-native-easy-grid";
import { ActivityIndicator, Dimensions, Image, RefreshControl, ScrollView, StyleSheet, Text, View } from 'react-native';
import { Appbar, Card, Paragraph, Title } from 'react-native-paper';
import NumberFormat from 'react-number-format';
import AsyncStorage from '@react-native-async-storage/async-storage';
import logo from '../assets/images/logo.png';
import Loading from '../components/Loading';
import languages from '../screensTranslations/Exchanges';
import Reload from '../components/Reload';
import {BASE_URL,FIRST_COLOR,SECOND_COLOR} from "@env";


class ExchangesScreen extends React.Component {
    componentDidMount() {
        this.setState({
            currentLang: this.props.screenProps.currentLang,
            currentCurrency: this.props.screenProps.currentCurrency,
            currentExchange: (this.props.screenProps.currentCurrency).toLowerCase()
        }, () =>{
            this.getData();
        });
    }
    componentDidUpdate(prevprops) {
        if(prevprops.checkPingRes != this.props.checkPingRes){
            if(this.props.checkPingRes == 'error'){
                this.setState({
                    getDataError: languages[5][this.state.currentLang],
                    isLoading: false
                });
            }
            else{
                if(this.props.checkPingRes.status_msg){
                    this.setState({
                        getDataError: this.props.checkPingRes.status_msg,
                        isLoading: false
                    });
                }
                else{
                    this.setState({ 
                        loadingStatus:0,
                        refresh:true,
                        currentPage: 1,
                        getExchanges:[],
                    }, () =>{
                        let vars = this.state;
                        this.props.getExchangeRates(vars);
                        this.props.getExchanges(vars);
                    });
                }
            }
        }
        if(prevprops.getExchangeRatesRes !== this.props.getExchangeRatesRes){
            if(this.props.getExchangeRatesRes == 'error'){
                this.setState({
                    getDataError: this.props.getExchangeRatesRes,
                    isLoading: false,
                    refresh: false
                });
            }
            else{
                if(this.props.getExchangeRatesRes.status_msg){
                    this.setState({
                        getDataError: this.props.getExchangeRatesRes.status_msg,
                        isLoading: false,
                        refresh: false
                    });
                }
                else{
                    let loadingStatus = this.state.loadingStatus;
                    loadingStatus += 1;
                    this.setState({ getDataError: '',getExchangeRates: this.props.getExchangeRatesRes.rates[this.state.currentExchange].value,loadingStatus:loadingStatus }, () =>{
                        if(loadingStatus >= 2){
                            this.setState({isLoading: false, refresh: false});
                        }
                    });
                }
            }
        }
        if(prevprops.getExchangesRes !== this.props.getExchangesRes){
            if(this.props.getExchangesRes == 'error'){
                this.setState({
                    getDataError: this.props.getExchangesRes,
                    isLoading: false,
                    refresh: false
                });
            }
            else{
                if(this.props.getExchangesRes.status_msg){
                    this.setState({
                        getDataError: this.props.getExchangesRes.status_msg,
                        isLoading: false,
                        refresh: false
                    });
                }
                else{
                    if(this.state.currentPage == 1){
                        let loadingStatus = this.state.loadingStatus;
                        loadingStatus += 1;
                        this.setState({ getDataError: '',getExchanges: this.props.getExchangesRes,loadingStatus:loadingStatus }, () =>{
                            if(loadingStatus >= 2){
                                this.setState({isLoading: false, refresh: false});
                            }
                        });
                    }
                    else{
                        if(this.props.getExchangesRes.length > 0){
                            let concatData = [...this.state.getExchanges, ...this.props.getExchangesRes];
                            this.setState({ getExchanges: concatData }, () => {
                                this.setState({moreLoading: false});
                            });
                        }
                        else{
                            this.setState({moreLoading: false});
                        }
                    }
                }
            }
        }
    }
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            refresh: false,
            moreLoading:false,
            currentLang: 'en',
            currentCurrency: 'USD',
            currentExchange: 'usd',
            loadingStatus:0,
            getExchangeRates: [],
            getExchanges: [],
            currentPage: 1,
            perPage: 30,
        };
        this.getData = this.getData.bind(this);
        this.getMoreData = this.getMoreData.bind(this);
        this.formatter = this.formatter.bind(this);
    }
    getData(){
        this.props.checkPing();
    }
    getMoreData(){
        if(this.state.moreLoading == false){
            let currentPage = this.state.currentPage;
            this.setState({ moreLoading:true, currentPage: (Number(currentPage)+1) }, () => {
                let vars = this.state;
                this.props.getExchanges(vars);
            });
        }
    }
    formatter(data,fontSize){
        let num = Math.round((data + Number.EPSILON) * 100) / 100;
        let res = (<NumberFormat 
                        displayType={'text'} 
                        thousandSeparator={true} 
                        value={num} 
                        renderText={(value, props) => <Text style={{textAlign:'right',fontSize:fontSize}}>{value}</Text> }/>);
        return res;
    }
    render() {
        if(this.state.isLoading == true){
            return (
                <Loading />
            );
        }
        else{
            let exchageRates = this.state.getExchangeRates;
            let exchanges = this.state.getExchanges;
            let currentCurrency = this.state.currentCurrency;
            let viewData;
            let moreData;
            if(exchanges.length > 0){
                viewData = (
                    <Grid>
                        <Row style={{height:40,paddingTop:10,backgroundColor:'#222'}}>
                            <Col style={{width:50}}><Text style={{fontSize:12,textAlign:'center',color:'#fff'}}>#</Text></Col>
                            <Col style={{width:170}}><Text style={{fontSize:12,textAlign:'left',color:'#fff'}}>{languages[0][this.state.currentLang]}</Text></Col>
                            <Col style={{width:50}}><Text style={{fontSize:12,textAlign:'center',color:'#fff'}}>{languages[1][this.state.currentLang]}</Text></Col>
                            <Col><Text style={{paddingRight:10,fontSize:12,textAlign:'right',color:'#fff'}}>{languages[2][this.state.currentLang]}</Text></Col>
                        </Row>
                        <ScrollView
                            onScroll={(e) => {
                                let paddingToBottom = 100;
                                paddingToBottom += e.nativeEvent.layoutMeasurement.height;
                                if((e.nativeEvent.contentOffset.y >= e.nativeEvent.contentSize.height - paddingToBottom)) {
                                    this.getMoreData();
                                }
                            }}
                            refreshControl={<RefreshControl refreshing={this.state.refresh} onRefresh={()=>this.getData()} />}
                        >
                        {
                            exchanges.map((item, i) => (
                                <Row key={'row_'+i} style={(i%2) ? (styles.rowsColor) : styles.rowsWhite}>
                                    <Col style={{width:50}}><Text style={{fontSize:12,textAlign:'center'}}>{(i+1)}</Text></Col>
                                    <Col style={{width:170,flexDirection:'row',justifyContent:'flex-start'}}>
                                        <Image source={{uri:item.image}} style={{width:20,height:20,marginRight:5,marginTop:0}}/>
                                        <Text style={{fontSize:12,textAlign:'left',}}>{item.name.toUpperCase()}</Text>
                                    </Col>
                                    <Col style={{width:50}}>
                                        <Text style={[styles.greenText, item.trust_score<=5 && styles.redText]}>
                                            {(item.trust_score) ? (item.trust_score) : '0'}
                                        </Text>
                                    </Col>
                                    <Col style={{paddingRight:10}}>
                                        {this.formatter((item.trade_volume_24h_btc*exchageRates),10)}
                                    </Col>
                                </Row>
                            ))
                        }
                        </ScrollView>
                    </Grid>
                );
            }
            let bottomLoading;
            if(this.state.moreLoading == true){
                bottomLoading = (
                    <ActivityIndicator />
                );
            }
            return (
                <View style={styles.wrapper}>
                    <Appbar.Header style={{backgroundColor:FIRST_COLOR}}>
                        <Appbar.Content titleStyle={{color:'#fff',textAlign:'center'}} title={languages[0][this.state.currentLang]} />
                    </Appbar.Header>
                    <View style={{flexDirection:'row',marginBottom:10}}>
                        <Card style={{marginRight:5,width:Dimensions.get('window').width * 0.5}}>
                            <Card.Content>
                                <Title style={{fontSize:14}}>{languages[3][this.state.currentLang]}</Title>
                                <Paragraph style={{color:'green'}}>{this.formatter(this.state.getExchanges.length,14)}</Paragraph>
                            </Card.Content>
                        </Card>
                        <Card style={{marginLeft:5,width:Dimensions.get('window').width * 0.5}}>
                            <Card.Content>
                            <Title style={{fontSize:14}}>{languages[4][this.state.currentLang]}</Title>
                                <Paragraph style={{color:'green'}}>{this.state.currentCurrency}</Paragraph>
                            </Card.Content>
                        </Card>
                    </View>
                    {viewData}
                    {bottomLoading}
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    wrapper:{
        flex:1,
    },
    header:{
        width: Dimensions.get('window').width * 1,
        backgroundColor:FIRST_COLOR,
        height:90,
        alignItems:'center'
    },
    logo:{
        width:305,
        height:87,
    },
    rowsWhite: { 
        height:50,paddingTop:10,borderTopWidth:1,borderTopColor:'#ddd',backgroundColor:'#fff'
    },
    rowsColor:{
        height:50,paddingTop:10,borderTopWidth:1,borderTopColor:'#ddd',backgroundColor: '#F7F6E7'
    },
    greenText: { 
        fontSize:12,textAlign:'center',color:'green'
    },
    redText: { 
        fontSize:12,textAlign:'center',color:'red'
    },
});

ExchangesScreen.navigationOptions = {
    header:null
};

function mapStateToProps(state, props) {
    return {
        checkPingRes: state.productReducer.checkPingRes,
        getExchangeRatesRes: state.productReducer.getExchangeRatesRes,
        getExchangesRes: state.productReducer.getExchangesRes,
    }
}
const mapDispatchToProps = {
    ...productActions
};
export default connect(mapStateToProps, mapDispatchToProps)(ExchangesScreen);