import React from 'react';
import { connect } from 'react-redux';
import * as productActions from '../actions/productActions';
import { Col, Row, Grid } from "react-native-easy-grid";
import { ActivityIndicator, Dimensions, Image, RefreshControl, ScrollView, StyleSheet, Text, View } from 'react-native';
import { Appbar, Card, Paragraph, Title } from 'react-native-paper';
import NumberFormat from 'react-number-format';
import AsyncStorage from '@react-native-async-storage/async-storage';
import logo from '../assets/images/logo.png';
import Loading from '../components/Loading';
import languages from '../screensTranslations/Categories';
import Reload from '../components/Reload';
import {BASE_URL,FIRST_COLOR,SECOND_COLOR} from "@env";


class CategoriesScreen extends React.Component {
    componentDidMount() {
        this.setState({
            currentLang: this.props.screenProps.currentLang,
            currentCurrency: this.props.screenProps.currentCurrency,
        }, () =>{
            this.getData();
        });
    }
    componentDidUpdate(prevprops) {
        if(prevprops.checkPingRes != this.props.checkPingRes){
            if(this.props.checkPingRes == 'error'){
                this.setState({
                    getDataError: languages[5][this.state.currentLang],
                    isLoading: false
                });
            }
            else{
                if(this.props.checkPingRes.status_msg){
                    this.setState({
                        getDataError: this.props.checkPingRes.status_msg,
                        isLoading: false
                    });
                }
                else{
                    this.setState({ 
                        currentPage: 1,
                        refresh: true,
                        getCategories: []
                    }, () =>{
                        let vars = this.state;
                        this.props.getCategories(vars);
                    });
                }
            }
        }
        if(prevprops.getCategoriesRes !== this.props.getCategoriesRes){
            if(this.props.getCategoriesRes == 'error'){
                this.setState({
                    getDataError: this.props.getCategoriesRes,
                    isLoading: false,
                    refresh: false
                });
            }
            else{
                if(this.props.getCategoriesRes.status_msg){
                    this.setState({
                        getDataError: this.props.getCategoriesRes.status_msg,
                        isLoading: false,
                        refresh: false
                    });
                }
                else{
                    this.setState({ getDataError: '',getCategories: this.props.getCategoriesRes,isLoading: false, refresh: false });
                }
            }
        }
    }
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            refresh: false,
            currentLang: 'en',
            currentCurrency: 'USD',
            search:'',
            getCategories: [],
            order: 'market_cap_desc',
        };
        this.getData = this.getData.bind(this);
        this.formatter = this.formatter.bind(this);
    }
    getData(){
        this.props.checkPing();
    }
    formatter(data,fontSize){
        let num = Math.round((data + Number.EPSILON) * 100) / 100;
        let res = (<NumberFormat 
                        displayType={'text'} 
                        thousandSeparator={true} 
                        value={num} 
                        renderText={(value, props) => <Text style={{textAlign:'right',fontSize:fontSize}}>{value}</Text> }/>);
        return res;
    }
    render() {
        if(this.state.isLoading == true){
            return (
                <Loading />
            );
        }
        else{
            let categories = this.state.getCategories;
            let currentCurrency = this.state.currentCurrency;
            let viewData;
            if(categories.length > 0){
                viewData = (
                    <Grid>
                        <Row style={{height:40,paddingTop:10,backgroundColor:'#222'}}>
                            <Col style={{width:50}}><Text style={{fontSize:12,textAlign:'center',color:'#fff'}}>#</Text></Col>
                            <Col style={{width:150}}><Text style={{fontSize:12,textAlign:'left',color:'#fff'}}>{languages[1][this.state.currentLang]}</Text></Col>
                            <Col style={{width:40}}><Text style={{fontSize:12,textAlign:'center',color:'#fff'}}>24H</Text></Col>
                            <Col><Text style={{paddingRight:10,fontSize:12,textAlign:'right',color:'#fff'}}>{languages[2][this.state.currentLang]}</Text></Col>
                        </Row>
                        <ScrollView
                            refreshControl={<RefreshControl refreshing={this.state.refresh} onRefresh={()=>this.getData()} />}
                        >
                        {
                            categories.map((item, i) => (
                                <Row key={'row_'+i} style={(i%2) ? (styles.rowsColor) : styles.rowsWhite}>
                                    <Col style={{width:50}}><Text style={{fontSize:12,textAlign:'center'}}>{(i+1)}</Text></Col>
                                    <Col style={{width:150}}>
                                        <Text style={{fontSize:12,textAlign:'left'}}>{item.name}</Text>
                                    </Col>
                                    <Col style={{width:40}}>
                                        <Text style={[styles.greenText, item.market_cap_change_24h<=0 && styles.redText]}>
                                            {(item.market_cap_change_24h) ? (item.market_cap_change_24h).toFixed(1)+'%' : '0.0%'}
                                        </Text>
                                    </Col>
                                    <Col style={{paddingRight:10}}>{this.formatter((item.market_cap),9)}</Col>
                                </Row>
                            ))
                        }
                        </ScrollView>
                    </Grid>
                );
            }
            return (
                <View style={styles.wrapper}>
                    <Appbar.Header style={{backgroundColor:FIRST_COLOR}}>
                        <Appbar.Content titleStyle={{color:'#fff',textAlign:'center'}} title={languages[0][this.state.currentLang]} />
                    </Appbar.Header>
                    <View style={{flexDirection:'row',marginBottom:10}}>
                        <Card style={{marginRight:5,width:Dimensions.get('window').width * 0.5}}>
                            <Card.Content>
                                <Title style={{fontSize:14}}>{languages[3][this.state.currentLang]}</Title>
                                <Paragraph style={{color:'green'}}>{this.formatter((this.state.getCategories.length),14)}</Paragraph>
                            </Card.Content>
                        </Card>
                        <Card style={{marginLeft:5,width:Dimensions.get('window').width * 0.5}}>
                            <Card.Content>
                            <Title style={{fontSize:14}}>{languages[4][this.state.currentLang]}</Title>
                                <Paragraph style={{color:'green'}}>{this.state.currentCurrency}</Paragraph>
                            </Card.Content>
                        </Card>
                    </View>
                    {viewData}
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    wrapper:{
        flex:1,
    },
    header:{
        width: Dimensions.get('window').width * 1,
        backgroundColor:FIRST_COLOR,
        height:90,
        alignItems:'center'
    },
    logo:{
        width:305,
        height:87,
    },
    rowsWhite: { 
        height:50,paddingTop:10,borderTopWidth:1,borderTopColor:'#ddd',backgroundColor:'#fff'
    },
    rowsColor:{
        height:50,paddingTop:10,borderTopWidth:1,borderTopColor:'#ddd',backgroundColor: '#F7F6E7'
    },
    greenText: { 
        fontSize:12,textAlign:'center',color:'green'
    },
    redText: { 
        fontSize:12,textAlign:'center',color:'red'
    },
});

CategoriesScreen.navigationOptions = {
    header:null
};

function mapStateToProps(state, props) {
    return {
        checkPingRes: state.productReducer.checkPingRes,
        getCategoriesRes: state.productReducer.getCategoriesRes,
    }
}
const mapDispatchToProps = {
    ...productActions
};
export default connect(mapStateToProps, mapDispatchToProps)(CategoriesScreen);