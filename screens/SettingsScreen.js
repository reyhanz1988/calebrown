import React from 'react';
import { connect } from 'react-redux';
import * as accountActions from '../actions/accountActions';
import { Image, StyleSheet, Text, View } from 'react-native';
import { Appbar,List } from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';
import shadow from '../assets/images/shadow.png';
import languages from '../screensTranslations/Settings';
import Loading from '../components/Loading';
import Reload from '../components/Reload';
import {FIRST_COLOR} from "@env";

class SettingsScreen extends React.Component {
    componentDidMount() {
        this.setState({
            currentLang: this.props.screenProps.currentLang,
            currentCurrency: this.props.screenProps.currentCurrency,
        }, () =>{
            this.getData();
        });
    }
    componentDidUpdate(prevprops) {
        if(prevprops.logoutReduxRes !== this.props.logoutReduxRes){
            this.props.screenProps.changeLang('en');
            this.props.screenProps.changeCurrencies('USD');
            AsyncStorage.clear().then(() => {
                this.props.navigation.navigate('AuthLoading');
            });
            this.setState({isLoading: false});
        }
    }
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            currentLang: '',
            getProfile: '',
            getDataError:'',
            cart:0
        };
        this.getData = this.getData.bind(this);
        this.Logout = this.Logout.bind(this);
        this.NavigateTo = this.NavigateTo.bind(this);
    }
    getData(){
        this.setState({isLoading: false});
        let vars = this.state;
    }
    Logout(){
        this.setState({ isLoading: true}, () => {
            let vars = this.state;
            this.props.logoutRedux(vars);
        });
    };
    NavigateTo(page){
        this.props.navigation.navigate(page);
    }
    render() {
        if(this.state.isLoading == true){
            return (
                <Loading />
            );
        }
        else if(this.state.getDataError != ''){
            return (
                <Reload errorMessage={this.state.getDataError} getData={this.getData} />
            );
        }
        else{
            let list = [
                {
                    title: languages[1][this.state.currentLang],
                    icon: 'flag',
                    listNav: 'ChangeLanguage'
                },
                {
                    title: languages[2][this.state.currentLang],
                    icon: 'cash-100',
                    listNav: 'ChangeCurrency'
                },
                {
                    title: languages[3][this.state.currentLang],
                    icon: 'earth',
                    listNav: 'GlobalData'
                },
            ];
            return (
                <View style={styles.wrapper}>
                    <Appbar.Header style={{backgroundColor:FIRST_COLOR}}>
                        <Appbar.Content titleStyle={{color:'#fff',textAlign:'center'}} title={languages[0][this.state.currentLang]} />
                    </Appbar.Header>
                    <View style={{padding:15,flex:1}}>
                        {
                            list.map((item, i) => (
                                <List.Item
                                    key={i}
                                    title={item.title}
                                    left={props => <List.Icon {...props} icon={item.icon} />}
                                    right={props => <List.Icon {...props} icon='chevron-right' />}
                                    onPress={() => this.NavigateTo(item.listNav)}
                                    style={{borderBottomWidth:1,borderBottomColor:'#eee'}}
                                />
                            ))
                        }
                        {/*<List.Item
                            title={languages[4][this.state.currentLang]}
                            left={props => <List.Icon {...props} icon='power' />}
                            right={props => <List.Icon {...props} icon='chevron-right' />}
                            onPress={()=>this.Logout()}
                        />*/}
                    </View>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        width:'100%',
        height:'100%'
    },
    scrollWrapper: {
        flex: 1,
        backgroundColor: 'transparent',
    },
    listContainerStyle:{
        backgroundColor: 'transparent',
    },
    listContentContainerStyle:{
        borderBottomWidth:1,
        borderBottomColor:'#ddd',
        paddingVertical:10
    },
});

SettingsScreen.navigationOptions = {
    header:null
};

function mapStateToProps(state, props) {
    return {
        logoutReduxRes: state.accountReducer.logoutReduxRes,
    }
}
const mapDispatchToProps = {
    ...accountActions,
};
export default connect(mapStateToProps, mapDispatchToProps)(SettingsScreen);