import React from 'react';
import { connect } from 'react-redux';
import * as productActions from '../actions/productActions';
import { Image, StyleSheet, ScrollView, Text, View } from 'react-native';
import { Appbar, Icon, List } from 'react-native-paper';
import AsyncStorage from '@react-native-async-storage/async-storage';
import languages from '../screensTranslations/ChangeCurrency';
import Loading from '../components/Loading';
import Reload from '../components/Reload';
import {FIRST_COLOR} from "@env";

class ChangeCurrencyScreen extends React.Component {
    componentDidMount() {
        this.setState({
            currentLang: this.props.screenProps.currentLang,
            currentCurrency: this.props.screenProps.currentCurrency,
        }, () =>{
            this.getData();
        });
    }   
    componentDidUpdate(prevprops) {
        if(prevprops.checkPingRes != this.props.checkPingRes){
            if(this.props.checkPingRes == 'error'){
                this.setState({
                    getDataError: languages[1][this.state.currentLang],
                    isLoading: false
                });
            }
            else{
                if(this.props.checkPingRes.status_msg){
                    this.setState({
                        getDataError: this.props.checkPingRes.status_msg,
                        isLoading: false
                    });
                }
                else{
                    let vars = this.state;
                    this.props.getCurrencies(vars);
                }
            }
        }
        if(prevprops.getCurrenciesRes != this.props.getCurrenciesRes){
            if(this.props.getCurrenciesRes == 'error'){
                this.setState({
                    getDataError: this.props.getCurrenciesRes,
                    isLoading: false
                });
            }
            else{
                if(this.props.getCurrenciesRes.status_msg){
                    this.setState({
                        getDataError: this.props.getCurrenciesRes.status_msg,
                        isLoading: false
                    });
                }
                else{
                    this.setState({ 
                        getCurrencies: this.props.getCurrenciesRes ,
                        isLoading:false
                    });
                }
            }
        }
    }
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            currentLang: 'en',
            currentCurrency:'USD',
            getCurrencies:[],
            getDataError:''
        };
        this.getData = this.getData.bind(this);
        this.saveForm = this.saveForm.bind(this);
    }
    getData(){
        this.props.checkPing();
    }
    saveForm(currency){
        this.setState({ 
            currentCurrency:currency,
        }, () =>{
            this.props.screenProps.changeCurrencies(currency);
        });
    }
    render() {
        if(this.state.isLoading == true){
            return (
                <Loading />
            );
        }
        else if(this.state.getDataError != ''){
            return (
                <Reload errorMessage={this.state.getDataError} getData={this.getData} />
            );
        }
        else{
            let getCurrencies = this.state.getCurrencies;
            let listViews = [];
            for(let i=0;i<getCurrencies.length;i++){
                let leftIcons = [];
                if(getCurrencies[i] == 'btc' 
                    || getCurrencies[i] == 'eth' 
                    || getCurrencies[i] == 'ltc' 
                    || getCurrencies[i] == 'bch'
                    || getCurrencies[i] == 'bnb'
                    || getCurrencies[i] == 'eos'
                    || getCurrencies[i] == 'xrp'
                    || getCurrencies[i] == 'xlm'
                    || getCurrencies[i] == 'link'
                    || getCurrencies[i] == 'dot'
                    || getCurrencies[i] == 'yfi'
                    || getCurrencies[i] == 'xdr'
                    || getCurrencies[i] == 'xag'
                    || getCurrencies[i] == 'xau'
                    || getCurrencies[i] == 'bits'
                    || getCurrencies[i] == 'sats'){
                        let iconUri = "https://cryptoicon-api.vercel.app/api/icon/"+getCurrencies[i];
                        leftIcons.push(
                            <View key={'cr_'+i} style={{width:50,alignItems:'center'}}>
                                <Image source={{uri:iconUri}} style={{width:30,height:30}}/>
                            </View>
                        );
                }
                else{
                    let flagUri = "https://flagcdn.com/28x21/"+getCurrencies[i].substring(0,2)+".png";
                    leftIcons.push(
                        <View key={'cr_'+i} style={{width:50,alignItems:'center'}}>
                            <Image source={{uri:flagUri}} style={{width:30,height:30}}/>
                        </View>
                    );
                }
                let bgColor;
                if(getCurrencies[i].toUpperCase() == this.state.currentCurrency){
                    bgColor = "#FADA5E";
                }
                else{
                    bgColor = "#fff";
                }
                listViews.push(
                    <List.Item
                        key={i}
                        title={getCurrencies[i].toUpperCase()}
                        left={props => leftIcons}
                        right={props => <List.Icon {...props} icon='chevron-right' />}
                        onPress={() => this.saveForm(getCurrencies[i].toUpperCase())}
                        style={{borderBottomWidth:1,borderBottomColor:'#eee',backgroundColor:bgColor}}
                    />
                );
            }
            return (
                <View style={styles.wrapper}>
                    <Appbar.Header style={{backgroundColor:FIRST_COLOR}}>
                        <Appbar.BackAction style={{marginTop:10}} color="#fff" onPress={() => this.props.navigation.goBack()} />
                        <Appbar.Content titleStyle={{color:'#fff',textAlign:'center'}} title={languages[0][this.state.currentLang]} />
                    </Appbar.Header>
                    <ScrollView>
                        {listViews}
                    </ScrollView>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        width:'100%',
        height:'100%'
    },
});

ChangeCurrencyScreen.navigationOptions = {
    header:null
};

function mapStateToProps(state, props) {
    return {
        checkPingRes: state.productReducer.checkPingRes,
        getCurrenciesRes: state.productReducer.getCurrenciesRes
    }
}
const mapDispatchToProps = {
    ...productActions
};
export default connect(mapStateToProps, mapDispatchToProps)(ChangeCurrencyScreen);