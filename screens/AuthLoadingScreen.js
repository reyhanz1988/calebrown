import React from 'react';
import {ActivityIndicator, StatusBar, Platform, StyleSheet, View} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import RNRestart from 'react-native-restart';
import Loading from '../components/Loading';

export default class AuthLoadingScreen extends React.Component {
    componentDidMount() {
        if(Platform.OS !== 'ios'){
            StatusBar.setBackgroundColor('#ffffff');
        }
        StatusBar.setBarStyle('dark-content');
        AsyncStorage.getItem('LANG').then((value) => {
            if (value) { 
                this.setState({currentLang: value}, () =>{
                    AsyncStorage.getItem('CURRENCIES').then((value) => {
                        if (value) { 
                            this.setState({currentCurrency: value}, () =>{
                                this.props.navigation.navigate('App');
                            });
                        } 
                    });
                });
            } 
            else {
                AsyncStorage.setItem('LANG', 'en').then((value) => {
                    this.setState({currentLang: 'en'}, () =>{
                        AsyncStorage.setItem('CURRENCIES', 'USD').then((value) => {
                            this.setState({currentCurrency: 'USD'}, () =>{
                                RNRestart.Restart();
                            });
                        });
                    });
                });
            }
        });
    }
    constructor(props) {
        super(props);
        this.state = {
            currentLang: 'en',
            currentCurrency: 'USD'
        };
    }
    render() {
        return (
            <Loading />
        );
    }
}