#!/bin/sh
cd android
./gradlew assembleRelease --stacktrace
mv app/build/outputs/apk/release/app-release.apk ~/Downloads/app-release.apk
rm ~/Downloads/app-release-aligned.apk
rm ~/Downloads/app-release-signed.apk
#$ANDROID_HOME/build-tools/29.0.2/zipalign -v -p 4 ~/Downloads/app-release.apk ~/Downloads/app-release-aligned.apk
cp ~/Downloads/app-release.apk ~/Downloads/app-release-aligned.apk
$ANDROID_HOME/build-tools/29.0.2/apksigner sign --ks-pass pass:calebrown --key-pass pass:calebrown --ks ~/Projects/project-archive/Calebrown/keystore/android/calebrown.keystore --out ~/Downloads/app-release-signed.apk ~/Downloads/app-release-aligned.apk 
