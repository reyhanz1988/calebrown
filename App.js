import React from 'react';
import { Platform, StatusBar, StyleSheet, View } from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AppNavigator from './navigation/AppNavigator';
import RNRestart from 'react-native-restart';
import { Provider } from 'react-redux';
import store from './store';
import Orientation from 'react-native-orientation-locker';

console.disableYellowBox = true; 

export default class App extends React.Component {
    componentDidMount() {
        Orientation.getAutoRotateState((rotationLock) => this.setState({rotationLock}));
        Orientation.lockToPortrait();
        AsyncStorage.getItem('LANG').then((value) => {
            this.setState({ currentLang: value });
        });
        AsyncStorage.getItem('CURRENCIES').then((value) => {
            this.setState({ currentCurrency: value });
        });
    }
    componentDidUpdate(prevprops) {
        
    }
    constructor(props) {
        super(props);
        this.state = {
            rotationLock: '',
            currentLang: 'en',
            currentCurrency: 'USD',
            isLoading: true,
        };
        this.changeLang = this.changeLang.bind(this);
        this.changeCurrencies = this.changeCurrencies.bind(this);
    }
    componentWillUnmount() {

    }
    changeLang(currentLang){
        AsyncStorage.setItem('LANG', currentLang).then((value) => {
            this.setState({
                currentLang: currentLang
            }, () =>{
                RNRestart.Restart();
            });
        });
    }
    changeCurrencies(currentCurrency){
        AsyncStorage.setItem('CURRENCIES', currentCurrency).then((value) => {
            this.setState({
                currentCurrency: currentCurrency
            }, () =>{
                RNRestart.Restart();
            });
        });
    }
    render() {
        let theApp;
        theApp = (
            <View style={styles.container}>
                {Platform.OS === 'ios' && <StatusBar />}
                <AppNavigator
                    screenProps={{  changeLang:this.changeLang,
                                    changeCurrencies:this.changeCurrencies,
                                    currentLang:this.state.currentLang,
                                    currentCurrency:this.state.currentCurrency
                                }}
                />
            </View>
        );
        return (
            <Provider store={store}>
                <View style={styles.container}>
                    {theApp}
                </View>
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    }
});
