import {
    CHECK_PING, GET_CURRENCIES, GET_GLOBAL, GET_COINS, SEARCH_COINS, GET_CATEGORIES, GET_EXCHANGES, GET_EXCHANGE_RATES, GET_DERIVATIVES
} from "../actions/productActions";

const initialState = {
    successMsg: '',
    errorMsg: '',
    checkPingRes:[],
    getCurrenciesRes:[],
    getGlobalRes:[],
    getCoinsRes:[],
    searchCoinsRes:[],
    getCategoriesRes:[],
    getExchangesRes:[],
    getExchangeRatesRes:[],
    getDerivativesRes:[],
};

const productReducer = (state = initialState, action) => {

    const { type, payload } = action;

    switch (type) {

        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        /*  CHECK_PING                                                                                                                              */
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        case CHECK_PING.REQUESTED:
            return { ...state };
        case CHECK_PING.SUCCESS:
            return {
                ...state,
                checkPingRes: payload.response,
                errorMsg: '',
            };
        case CHECK_PING.ERROR:
            return { ...state, errorMsg: payload.error };

        /*------------------------------------------------------------------------------------------------------------------------------------------*/

        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        /*  GET_CURRENCIES                                                                                                                          */
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        case GET_CURRENCIES.REQUESTED:
            return { ...state };
        case GET_CURRENCIES.SUCCESS:
            return {
                ...state,
                getCurrenciesRes: payload.response,
                errorMsg: '',
            };
        case GET_CURRENCIES.ERROR:
            return { ...state, errorMsg: payload.error };

        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        /*  GET_GLOBAL                                                                                                                          */
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        case GET_GLOBAL.REQUESTED:
            return { ...state };
        case GET_GLOBAL.SUCCESS:
            return {
                ...state,
                getGlobalRes: payload.response,
                errorMsg: '',
            };
        case GET_GLOBAL.ERROR:
            return { ...state, errorMsg: payload.error };

        /*------------------------------------------------------------------------------------------------------------------------------------------*/

        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        /*  GET_COINS                                                                                                                               */
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        case GET_COINS.REQUESTED:
            return { ...state };
        case GET_COINS.SUCCESS:
            return {
                ...state,
                getCoinsRes: payload.response,
                errorMsg: '',
            };
        case GET_COINS.ERROR:
            return { ...state, errorMsg: payload.error };

        /*------------------------------------------------------------------------------------------------------------------------------------------*/

        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        /*  SEARCH_COINS                                                                                                                            */
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        case SEARCH_COINS.REQUESTED:
            return { ...state };
        case SEARCH_COINS.SUCCESS:
            return {
                ...state,
                searchCoinsRes: payload.response,
                errorMsg: '',
            };
        case SEARCH_COINS.ERROR:
            return { ...state, errorMsg: payload.error };

        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        /*  GET_CATEGORIES                                                                                                                          */
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        case GET_CATEGORIES.REQUESTED:
            return { ...state };
        case GET_CATEGORIES.SUCCESS:
            return {
                ...state,
                getCategoriesRes: payload.response,
                errorMsg: '',
            };
        case GET_CATEGORIES.ERROR:
            return { ...state, errorMsg: payload.error };

        /*------------------------------------------------------------------------------------------------------------------------------------------*/

        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        /*  GET_EXCHANGES                                                                                                                           */
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        case GET_EXCHANGES.REQUESTED:
            return { ...state };
        case GET_EXCHANGES.SUCCESS:
            return {
                ...state,
                getExchangesRes: payload.response,
                errorMsg: '',
            };
        case GET_EXCHANGES.ERROR:
            return { ...state, errorMsg: payload.error };

        /*------------------------------------------------------------------------------------------------------------------------------------------*/

        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        /*  GET_EXCHANGE_RATES                                                                                                                      */
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        case GET_EXCHANGE_RATES.REQUESTED:
            return { ...state };
        case GET_EXCHANGE_RATES.SUCCESS:
            return {
                ...state,
                getExchangeRatesRes: payload.response,
                errorMsg: '',
            };
        case GET_EXCHANGE_RATES.ERROR:
            return { ...state, errorMsg: payload.error };

        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        /*  GET_DERIVATIVES                                                                                                                           */
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        case GET_DERIVATIVES.REQUESTED:
            return { ...state };
        case GET_DERIVATIVES.SUCCESS:
            return {
                ...state,
                getDerivativesRes: payload.response,
                errorMsg: '',
            };
        case GET_DERIVATIVES.ERROR:
            return { ...state, errorMsg: payload.error };

        /*------------------------------------------------------------------------------------------------------------------------------------------*/

        default:
            return state
    }
}

function cloneObject(object){
    return JSON.parse(JSON.stringify(object));
}

function getIndex(data, id){
    let clone = JSON.parse(JSON.stringify(data));
    return clone.findIndex((obj) => parseInt(obj.id) === parseInt(id));
}


export default productReducer