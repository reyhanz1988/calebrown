import {GET_LANG, POST_LANG} from "../actions/masterActions";

const initialState = {
    successMsg: '',
    errorMsg: '',
    getLangRes:[],
    postLangRes:[]
};

const masterReducer = (state = initialState, action) => {

    const { type, payload } = action;

    switch (type) {

        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        /*  GET_LANG                                                                                                                                */
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        case GET_LANG.REQUESTED:
            return { ...state };
        case GET_LANG.SUCCESS:
            return {
                ...state,
                getLangRes: payload.response,
                errorMsg: '',
            };
        case GET_LANG.ERROR:
            return { ...state, errorMsg: payload.error };

        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        /*  POST_LANG                                                                                                                               */
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        case POST_LANG.SUCCESS:
            return {
                ...state,
                postLangRes: payload,
                errorMsg: '',
            };
        
        /*------------------------------------------------------------------------------------------------------------------------------------------*/
        default:
            return state
    }
}

function cloneObject(object){
    return JSON.parse(JSON.stringify(object));
}

function getIndex(data, id){
    let clone = JSON.parse(JSON.stringify(data));
    return clone.findIndex((obj) => parseInt(obj.id) === parseInt(id));
}


export default masterReducer