module.exports =  [
/*0*/
{
    en: "Global Cryptocurrencies Data",
    id: "Cryptocurrency Global Data",

},

/*1*/
{
    en: "Active Cryptocurrencies",
    id: "Cryptocurrency Aktif",

},

/*2*/
{
    en: "Markets",
    id: "Pasar",

},

/*3*/
{
    en: "Upcoming Icos",
    id: "Icos yang akan datang",

},

/*4*/
{
    en: "Ongoing Icos",
    id: "Icos Berjalan",

},

/*5*/
{
    en: "Ended Icos",
    id: "Icos Berakhir",

},

/*6*/
{
    en: "Last Update",
    id: "Terakhir Update",

},

/*7*/
{
    en: "Total Market Cap",
    id: "Total Kapitalisasi Pasar",

},

/*8*/
{
    en: "Market Cap %",
    id: "% Kapitalisasi Pasar",

},

/*9*/
{
    en: "Total Volume",
    id: "Total Volume",

},

/*10*/
{
    en: "Server currently very busy and unavailable",
    id: "Server saat ini sangat sibuk dan tidak tersedia",
    
},

];