module.exports =  [
/*0*/
{
    en: "Derivatives",
    id: "Derivatif",
    
},

/*1*/
{
    en: "Open Interest",
    id: "Minat Beli",
    
},

/*2*/
{
    en: "Volume",
    id: "Volume",
    
},

/*3*/
{
    en: "Data Loaded",
    id: "Data Dimuat",
    
},

/*4*/
{
    en: "Currency",
    id: "Mata Uang",
    
},

/*5*/
{
    en: "Server currently very busy and unavailable",
    id: "Server saat ini sangat sibuk dan tidak tersedia",
    
},

];