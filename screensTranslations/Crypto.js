module.exports =  [
/*0*/
{
    en: "Cryptocurrency",
    id: "Cryptocurrency",
    
},

/*1*/
{
    en: "Coin",
    id: "Koin",
    
},

/*2*/
{
    en: "Price",
    id: "Harga",
    
},

/*3*/
{
    en: "Market Cap",
    id: "Kapitalisasi Pasar",
    
},

/*4*/
{
    en: "Search More Data",
    id: "Cari Lebih Banyak",
    
},

/*5*/
{
    en: "Load More Data",
    id: "Muat Lebih Banyak",
    
},

/*6*/
{
    en: "Active Cryptocurrencies",
    id: "Cryptocurrency Aktif",
    
},

/*7*/
{
    en: "Currency",
    id: "Mata Uang",
    
},

/*8*/
{
    en: "Search",
    id: "Cari",
    
},

/*9*/
{
    en: "Server currently very busy and unavailable",
    id: "Server saat ini sangat sibuk dan tidak tersedia",
    
},
];