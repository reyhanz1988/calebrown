module.exports =  [
/*0*/
{
    en: "Categories",
    id: "Kategori",
    
},

/*1*/
{
    en: "Category",
    id: "Kategori",
    
},

/*2*/
{
    en: "Market Cap",
    id: "Kapitalisasi Pasar",
    
},

/*3*/
{
    en: "Data Loaded",
    id: "Data Dimuat",
    
},

/*4*/
{
    en: "Currency",
    id: "Mata Uang",
    
},

/*5*/
{
    en: "Server currently very busy and unavailable",
    id: "Server saat ini sangat sibuk dan tidak tersedia",
    
},

];