import {BASE_URL,API_COINGECKO} from "@env"

let ProductApi = {
    checkPing(vars) {
        let params = {
            method: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        };
        return fetch(API_COINGECKO + '/api/v3/ping', params)
            .then((res) => res.json())
            .catch((error) => {
                return 'error';
                console.warn("checkPing => " + error);
            });
    },
    getCurrencies(vars) {
        let params = {
            method: 'GET',
            headers: {
                'Accept-Language': vars.currentLang,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        };
        return fetch(API_COINGECKO + '/api/v3/simple/supported_vs_currencies', params)
            .then((res) => res.json())
            .catch((error) => {
                return 'error';
                console.warn("getCurrencies => " + error);
            });
    },
    getGlobal(vars) {
        let params = {
            method: 'GET',
            headers: {
                'Accept-Language': vars.currentLang,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        };
        return fetch(API_COINGECKO + '/api/v3/global', params)
            .then((res) => res.json())
            .catch((error) => {
                return 'error';
                console.warn("getGlobal => " + error);
            });
    },
    getCoins(vars) {
        let params = {
            method: 'GET',
            headers: {
                'Accept-Language': vars.currentLang,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        };
        return fetch(API_COINGECKO + '/api/v3/coins/markets?vs_currency='+(vars.currentCurrency).toLowerCase()+'&order='+vars.order+'&per_page='+vars.perPage+'&page='+vars.currentPage+'&sparkline=false', params)
            .then((res) => res.json())
            .catch((error) => {
                return 'error';
                console.warn("getCoins => " + error);
            });
    },
    searchCoins(vars) {
        let params = {
            method: 'GET',
            headers: {
                'Accept-Language': vars.currentLang,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        };
        return fetch(API_COINGECKO + '/api/v3/coins/markets?vs_currency='+(vars.currentCurrency).toLowerCase()+'&order='+vars.order+'&per_page='+vars.searchPerPage+'&page='+vars.searchPage+'&sparkline=false', params)
            .then((res) => res.json())
            .catch((error) => {
                return 'error';
                console.warn("searchCoins => " + error);
            });
    },
    getCategories(vars) {
        let params = {
            method: 'GET',
            headers: {
                'Accept-Language': vars.currentLang,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        };
        return fetch(API_COINGECKO + '/api/v3/coins/categories?order='+vars.order, params)
            .then((res) => res.json())
            .catch((error) => {
                return 'error';
                console.warn("getCategories => " + error);
            });
    },
    getExchanges(vars) {
        let params = {
            method: 'GET',
            headers: {
                'Accept-Language': vars.currentLang,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        };
        return fetch(API_COINGECKO + '/api/v3/exchanges?&per_page='+vars.perPage+'&page='+vars.currentPage, params)
            .then((res) => res.json())
            .catch((error) => {
                return 'error';
                console.warn("getExchanges => " + error);
            });
    },
    getExchangeRates(vars) {
        let params = {
            method: 'GET',
            headers: {
                'Accept-Language': vars.currentLang,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        };
        return fetch(API_COINGECKO + '/api/v3/exchange_rates', params)
            .then((res) => res.json())
            .catch((error) => {
                return 'error';
                console.warn("getExchangeRates => " + error);
            });
    },
    getDerivatives(vars) {
        let params = {
            method: 'GET',
            headers: {
                'Accept-Language': vars.currentLang,
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
        };
        return fetch(API_COINGECKO + '/api/v3/derivatives/exchanges?&per_page='+vars.perPage+'&page='+vars.currentPage+'&order='+vars.order, params)
            .then((res) => res.json())
            .catch((error) => {
                return 'error';
                console.warn("getDerivatives => " + error);
            });
    },
};

module.exports = ProductApi;
