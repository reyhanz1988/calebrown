//npm run test -- -i -u
import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { create, act } from 'react-test-renderer';
import { cleanup, fireEvent, render, waitFor } from '@testing-library/react-native';
import SettingsScreen from '../screens/SettingsScreen';
import Reload from '../components/Reload';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import store from '../store';
import rootReducer from '../reducers/rootReducer';
import accountReducer from '../reducers/accountReducer';

jest.spyOn(console, 'warn').mockImplementation(() => {})
jest.spyOn(console, 'error').mockImplementation(() => {})

fetch = jest.fn(() => Promise.resolve());

it('checks if Async Storage is used', async () => {
    await AsyncStorage.setItem('LANG', 'en');
    let asyncLang = await AsyncStorage.getItem('LANG');
    expect(AsyncStorage.getItem).toBeCalledWith('LANG');
    await AsyncStorage.setItem('CURRENCIES', 'USD');
    let asyncCurrency = await AsyncStorage.getItem('CURRENCIES');
    expect(AsyncStorage.getItem).toBeCalledWith('CURRENCIES');
})

const tree = create(
    <Provider store={store}>
        <SettingsScreen 
            screenProps={{  
                currentLang:'en',
                currentCurrency:'USD',
            }}
        />
    </Provider>
);

describe('SettingsScreen',()=>{
    it('SettingsScreen should render properly', () => {
        act(() => {
            expect(tree).toMatchSnapshot();
        });
    });
})

describe('accountReducer', () => {
    it('accountReducer => initial state', () => {
        expect(rootReducer(accountReducer, {})).toMatchSnapshot()
    })
    it('accountReducer => LOGOUT_REDUX', () => {
        act(() => {
            expect(rootReducer(accountReducer, {type: 'LOGOUT_REDUX'})).toMatchSnapshot()
        });
    })
});