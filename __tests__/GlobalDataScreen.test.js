//npm run test -- -i -u
import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { create, act } from 'react-test-renderer';
import { cleanup, fireEvent, render, waitFor } from '@testing-library/react-native';
import GlobalDataScreen from '../screens/GlobalDataScreen';
import Reload from '../components/Reload';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import store from '../store';
import rootReducer from '../reducers/rootReducer';
import productReducer from '../reducers/productReducer';

jest.spyOn(console, 'warn').mockImplementation(() => {})
jest.spyOn(console, 'error').mockImplementation(() => {})

fetch = jest.fn(() => Promise.resolve());

it('checks if Async Storage is used', async () => {
    await AsyncStorage.setItem('LANG', 'en');
    let asyncLang = await AsyncStorage.getItem('LANG');
    expect(AsyncStorage.getItem).toBeCalledWith('LANG');
    await AsyncStorage.setItem('CURRENCIES', 'USD');
    let asyncCurrency = await AsyncStorage.getItem('CURRENCIES');
    expect(AsyncStorage.getItem).toBeCalledWith('CURRENCIES');
})

const tree = create(
    <Provider store={store}>
        <GlobalDataScreen 
            screenProps={{  
                currentLang:'en',
                currentCurrency:'USD',
            }}
        />
    </Provider>
);

describe('GlobalDataScreen',()=>{
    it('GlobalDataScreen should render properly', () => {
        act(() => {
            expect(tree).toMatchSnapshot();
        });
    });
})

describe('productReducer', () => {
    it('productReducer => initial state', () => {
        expect(rootReducer(productReducer, {})).toMatchSnapshot()
    })
    it('productReducer => CHECK_PING', () => {
        act(() => {
            expect(rootReducer(productReducer, {type: 'CHECK_PING'})).toMatchSnapshot()
        });
    })
    it('productReducer => GET_GLOBAL', () => {
        act(() => {
            expect(rootReducer(productReducer, {type: 'GET_GLOBAL'})).toMatchSnapshot()
        });
    })
});