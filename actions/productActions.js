import ProductApi from '../api/ProductApi';
import { createAsyncActionType, errorToastMessage } from '../components/util';

// checkPing
export const CHECK_PING = createAsyncActionType('CHECK_PING');
export const checkPing = vars => dispatch => {
    dispatch({
        type: CHECK_PING.REQUESTED,
    });
    ProductApi.checkPing(vars)
    .then(response => {
        dispatch({
            type: CHECK_PING.SUCCESS,
            payload: { response },
        });
    })
    .catch(error => {
        dispatch({
            type: CHECK_PING.ERROR,
            payload: { error },
        });
        errorToastMessage();
    });
};

// getCurrencies
export const GET_CURRENCIES = createAsyncActionType('GET_CURRENCIES');
export const getCurrencies = vars => dispatch => {
    dispatch({
        type: GET_CURRENCIES.REQUESTED,
    });
    ProductApi.getCurrencies(vars)
    .then(response => {
        dispatch({
            type: GET_CURRENCIES.SUCCESS,
            payload: { response },
        });
    })
    .catch(error => {
        dispatch({
            type: GET_CURRENCIES.ERROR,
            payload: { error },
        });
        errorToastMessage();
    });
};

// getGlobal
export const GET_GLOBAL = createAsyncActionType('GET_GLOBAL');
export const getGlobal = vars => dispatch => {
    dispatch({
        type: GET_GLOBAL.REQUESTED,
    });
    ProductApi.getGlobal(vars)
    .then(response => {
        dispatch({
            type: GET_GLOBAL.SUCCESS,
            payload: { response },
        });
    })
    .catch(error => {
        dispatch({
            type: GET_GLOBAL.ERROR,
            payload: { error },
        });
        errorToastMessage();
    });
};

// getCoins
export const GET_COINS = createAsyncActionType('GET_COINS');
export const getCoins = vars => dispatch => {
    dispatch({
        type: GET_COINS.REQUESTED,
    });
    ProductApi.getCoins(vars)
    .then(response => {
        dispatch({
            type: GET_COINS.SUCCESS,
            payload: { response },
        });
    })
    .catch(error => {
        dispatch({
            type: GET_COINS.ERROR,
            payload: { error },
        });
        errorToastMessage();
    });
};

// searchCoins
export const SEARCH_COINS = createAsyncActionType('SEARCH_COINS');
export const searchCoins = vars => dispatch => {
    dispatch({
        type: SEARCH_COINS.REQUESTED,
    });
    ProductApi.searchCoins(vars)
    .then(response => {
        dispatch({
            type: SEARCH_COINS.SUCCESS,
            payload: { response },
        });
    })
    .catch(error => {
        dispatch({
            type: SEARCH_COINS.ERROR,
            payload: { error },
        });
        errorToastMessage();
    });
};

// getCategories
export const GET_CATEGORIES = createAsyncActionType('GET_CATEGORIES');
export const getCategories = vars => dispatch => {
    dispatch({
        type: GET_CATEGORIES.REQUESTED,
    });
    ProductApi.getCategories(vars)
    .then(response => {
        dispatch({
            type: GET_CATEGORIES.SUCCESS,
            payload: { response },
        });
    })
    .catch(error => {
        dispatch({
            type: GET_CATEGORIES.ERROR,
            payload: { error },
        });
        errorToastMessage();
    });
};

// getExchanges
export const GET_EXCHANGES = createAsyncActionType('GET_EXCHANGES');
export const getExchanges = vars => dispatch => {
    dispatch({
        type: GET_EXCHANGES.REQUESTED,
    });
    ProductApi.getExchanges(vars)
    .then(response => {
        dispatch({
            type: GET_EXCHANGES.SUCCESS,
            payload: { response },
        });
    })
    .catch(error => {
        dispatch({
            type: GET_EXCHANGES.ERROR,
            payload: { error },
        });
        errorToastMessage();
    });
};

// getExchangeRates
export const GET_EXCHANGE_RATES = createAsyncActionType('GET_EXCHANGE_RATES');
export const getExchangeRates = vars => dispatch => {
    dispatch({
        type: GET_EXCHANGE_RATES.REQUESTED,
    });
    ProductApi.getExchangeRates(vars)
    .then(response => {
        dispatch({
            type: GET_EXCHANGE_RATES.SUCCESS,
            payload: { response },
        });
    })
    .catch(error => {
        dispatch({
            type: GET_EXCHANGE_RATES.ERROR,
            payload: { error },
        });
        errorToastMessage();
    });
};

// getDerivatives
export const GET_DERIVATIVES = createAsyncActionType('GET_DERIVATIVES');
export const getDerivatives = vars => dispatch => {
    dispatch({
        type: GET_DERIVATIVES.REQUESTED,
    });
    ProductApi.getDerivatives(vars)
    .then(response => {
        dispatch({
            type: GET_DERIVATIVES.SUCCESS,
            payload: { response },
        });
    })
    .catch(error => {
        dispatch({
            type: GET_DERIVATIVES.ERROR,
            payload: { error },
        });
        errorToastMessage();
    });
};